#Polygon бэкенд

###Установка

Для установки начальных разрешений и ролей выполнить:

`npm run rbac:build`

Это команда создаст папку `rbac-bootstrap` с единственным файлом `rbac.run-once.js`.

Затем выполнить:

`npm run rbac:setup`

Эта команда создаст недостающие права `create`, `read`, `update`, `delete` и `list` для коллекций `task`, `user`, `permission` и `role`.

###Сборка для разработки

Выполнить:

`npm run dev:build`

Команда создаст папку `dist` с файлом `index.bundle.js`.

Затем запустить билд командой:

`npm run dev`
