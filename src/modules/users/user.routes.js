import { Router } from 'express';

import { authLocal, authJwt } from '../../services/auth.service';
import * as userController from './user.controller';
import * as userValidation from './user.validation';
import * as AccessControl from '../../services/access-control.service';

const routes = new Router();

/**
 * Регистрация Юзера.
 *
 * @req.body.email - string, email, required, unique.
 * @req.body.userName - string, unique, required.
 * @req.body.firstName - string, required.
 * @req.body.lastName - string, required.
 * @req.body.password - string, required, not less one of: number, lowercase, uppercase.
 */
routes.post('/signup', userValidation.signUp, userController.signUp);

/**
 * Логин Юзера.
 *
 * @req.body.email - email or Username, string, required.
 * @req.body.password - string, required.
 */
routes.post('/login', userValidation.login, authLocal, userController.login);

/**
 * Получить список Юзеров.
 *
 * @req.query.skip - integer, positive or zero.
 * @req.query.limit - integer, positive or zero.
 */
routes.get(
  '/',
  authJwt,
  AccessControl.HasAnyRole(['superadmin', 'admin', 'readonly']),
  userValidation.getUsersList,
  userController.getUsersList,
);

/**
 * Получить Юзера по Id.
 *
 * @req.params.id - MongoId, required.
 */
routes.get(
  '/:id/',
  authJwt,
  AccessControl.HasAnyRole(['superadmin', 'admin', 'readonly']),
  userValidation.checkUserID('Request for get User by Id is invalid.'),
  userController.getUserById,
);

/**
 * Обновить Юзера, кроме Ролей.
 *
 * @req.body.email - string, email, optional, unique.
 * @req.body.userName - string, unique, optional.
 * @req.body.firstName - string, optional.
 * @req.body.lastName - string, optional.
 * @req.body.password - string, optional, not less one of: number, lowercase, uppercase.
 */
routes.patch(
  '/:id/',
  authJwt,
  AccessControl.HasAnyRole(['superadmin', 'admin']),
  userValidation.checkUserID('Request for update User is invalid.'),
  userValidation.updateUser,
  userController.updateUser,
);

/**
 * Добавить Юзеру Роли.
 *
 * @req.body.roles - array of roles.
 */
routes.patch(
  '/:id/add-roles',
  authJwt,
  AccessControl.HasAnyRole(['superadmin', 'admin']),
  userValidation.checkRoles,
  userController.transformRolesSkipInvalid,
  userController.updateUserRoles,
);

/**
 * Удалить Роли у Юзера.
 *
 * @req.body.roles - array of roles.
 */
routes.patch(
  '/:id/remove-roles',
  authJwt,
  AccessControl.HasAnyRole(['superadmin', 'admin']),
  userValidation.checkRoles,
  userController.transformRolesSkipInvalid,
  userController.removeRolesFromUser,
);

/**
 * Удалить Юзера по Id.
 *
 * @req.params.id - MongoId, required.
 */
routes.delete(
  '/:id/',
  authJwt,
  AccessControl.HasAnyRole(['superadmin', 'admin']),
  userValidation.checkUserID('Request for delete User is invalid.'),
  userController.deleteUser,
);

export default routes;
