import Router from 'express';

import { authJwt } from '../../services/auth.service';
import * as permissionController from './permission.controller';
import * as permissionValidation from './permission.validation';
import * as AccessControl from '../../services/access-control.service';

const routes = new Router();

/**
 * Создаёт Разрешение.
 *
 * @req.body.permission - object, required.
 * @req.body.permission.action - string, required, min = 3.
 * @req.body.permission.subject - string, required, min = 3.
 *
 * !!!OR!!!
 *
 * @req.body.permissions - array of objects
 *      action - string, required, min = 3.
 *      subject - string, required, min = 3.
 */
routes.post(
  '/',
  authJwt,
  AccessControl.HasRole('superadmin'),
  permissionValidation.createPermission,
  permissionController.createPermission,
);

/**
 * Обновляет Разрешение.
 *
 * @req.body.action - string, min = 3.
 *
 * !!!OR/AND!!!
 *
 * @req.body.subject - string, min = 3.
 */
routes.patch(
  '/:id',
  authJwt,
  AccessControl.HasRole('superadmin'),
  permissionValidation.updatePermission,
  permissionController.updatePermission,
);

/**
 * Ищет Роли по параметрам action и subject.
 *
 * @req.body.action - string, required, min = 3.
 * @req.body.subject - string, required, min = 3.
 */
routes.get(
  '/roles-by-permission',
  authJwt,
  AccessControl.HasAnyRole(['superadmin', 'admin', 'readonly']),
  permissionValidation.getRolesByPermission,
  permissionController.getRolesByPermission,
);

/**
 * Отдаёт Разрешение по его ID.
 *
 * @req.params.id - MongoId, required.
 */
routes.get(
  '/:id',
  authJwt,
  AccessControl.HasAnyRole(['superadmin', 'readonly']),
  permissionValidation.getPermissionById,
  permissionController.getPermissionById,
);

/**
 * Отдаёт список разрешений.
 *
 * @req.query.skip - integer, positive or zero.
 * @req.query.limit - integer, positive or zero.
 */
routes.get(
  '/',
  authJwt,
  AccessControl.HasAnyRole(['superadmin', 'readonly']),
  permissionValidation.getPermissionsList,
  permissionController.getPermissionsList,
);

/**
 * Удаляет разрешение.
 *
 * @req.body.action - string, required, min = 3.
 * @req.body.subject - string, required, min = 3.
 */
routes.delete(
  '/delete-permission',
  authJwt,
  AccessControl.HasRole('superadmin'),
  permissionValidation.deletePermission,
  permissionController.deletePermission,
);

export default routes;
