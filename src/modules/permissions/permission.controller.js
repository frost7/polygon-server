/* eslint-disable no-param-reassign */
import HTTPStatus from 'http-status';

import Permission from './permission.model';
import Role from '../roles/role.model';
import { asyncForEach } from '../../helpers/async-foreach';

export async function createPermission(req, res) {
  try {
    // Если создаём одно разрешение.
    if (req.body.permission) {
      const exist = await Permission.findByActionAndSubject(
        req.body.permission,
      ).catch(() => new Error('Can not check if permission exists already.'));
      if (exist) {
        return res.status(HTTPStatus.CREATED).json(exist);
      }
      const newPermission = await Permission.create(req.body.permission).catch(
        () => new Error('Can not create new permission.'),
      );
      return res.status(HTTPStatus.CREATED).json(newPermission);
    }
    // Если мы оказались тут, значит создаём Разрешения пакетно.
    const result = [];
    const perms = req.body.permissions;
    if (perms.length === 0) return res.status(HTTPStatus.CREATED).json([]);
    await asyncForEach(perms, async item => {
      const exist = await Permission.findByActionAndSubject(item).catch(
        () => new Error('Can not check if permission exists already.'),
      );
      if (exist) {
        result.push(exist);
      } else {
        const newPermission = await Permission.create(item).catch(
          () => new Error('Can not create new permission.'),
        );
        result.push(newPermission);
      }
    });
    return res.status(HTTPStatus.CREATED).json(result);
  } catch (err) {
    return res.status(HTTPStatus.BAD_REQUEST).json(err);
  }
}

export async function updatePermission(req, res) {
  try {
    const targetPermission = req._permission
      ? req._permission
      : await Permission.findById(req.params.id).catch(
          () =>
            new Error(`Can not find permission with id === ${req.params.id}.`),
        );
    const resultSupposedPermission = {
      action: targetPermission.action,
      subject: targetPermission.subject,
      ...req.body,
    };
    const duplicate = await Permission.findByActionAndSubject(
      resultSupposedPermission,
    ).catch('Can not check if updating permission duplicates exist.');
    if (duplicate) {
      return res.status(HTTPStatus.CONFLICT).json(duplicate.toJSON());
    }

    targetPermission.action = resultSupposedPermission.action;
    targetPermission.subject = resultSupposedPermission.subject;
    const result = await targetPermission.save();
    return res.status(HTTPStatus.OK).json(result.toJSON());
  } catch (err) {
    return res.status(HTTPStatus.BAD_REQUEST).json(err);
  }
}

export async function getPermissionById(req, res) {
  try {
    const permission = req._permission
      ? req._permission
      : await Permission.findById(req.params.id).catch(
          () =>
            new Error(`Can not find permission with id === ${req.params.id}.`),
        );
    return res.status(HTTPStatus.OK).json(permission.toJSON());
  } catch (err) {
    return res.status(HTTPStatus.BAD_REQUEST).json(err);
  }
}

export async function getPermissionsList(req, res) {
  try {
    const limit = parseInt(req.query.limit, 10);
    const skip = parseInt(req.query.skip, 10);
    const permissions = await Permission.list({ limit, skip }).catch(
      () => new Error(`Can not get list of Permissions.`),
    );
    return res.status(HTTPStatus.OK).json(permissions);
  } catch (err) {
    return res.status(HTTPStatus.BAD_REQUEST).json(err);
  }
}

export async function getRolesByPermission(req, res) {
  try {
    const permission = await Permission.findByActionAndSubject({
      action: req.body.action,
      subject: req.body.subject,
    }).catch(
      () => new Error(`Can not find Permission by its action and subject.`),
    );
    if (!permission) {
      return res.status(HTTPStatus.NOT_FOUND).json({
        message: `Permission [ '${req.body.action}', '${
          req.body.subject
        }' ] not found.`,
        status: 'Failed',
      });
    }
    let roles;
    if (permission) {
      roles = await Role.findRolesByPermission(permission._id).catch(
        () => new Error(`Can not find Roles by Permission.`),
      );
    } else {
      roles = [];
    }
    return res.status(HTTPStatus.OK).json(roles);
  } catch (err) {
    return res.status(HTTPStatus.BAD_REQUEST).json(err);
  }
}

export async function deletePermission(req, res) {
  try {
    const permission = await Permission.findOne({
      action: req.body.action,
      subject: req.body.subject,
    }).catch(
      () => new Error(`Can not get Permission by its action and subject.`),
    );
    if (!permission) {
      return res.status(HTTPStatus.NOT_FOUND).json({
        message: `Permission [ '${req.body.action}', '${
          req.body.subject
        }' ] not found.`,
        status: 'Failed',
      });
    }
    await permission.remove();
    return res.status(HTTPStatus.OK).json({
      message: `Permission [ '${req.body.action}', '${
        req.body.subject
      }' ] successfully deleted.`,
      status: 'OK',
    });
  } catch (err) {
    return res.status(HTTPStatus.BAD_REQUEST).json(err);
  }
}
