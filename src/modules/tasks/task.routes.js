import Router from 'express';

import { authJwt } from '../../services/auth.service';
import * as taskValidation from './task.validation';
import * as taskController from './task.controller';
import * as AccessControl from '../../services/access-control.service';

const routes = new Router();

/**
 * Добавить Файл в задачу.
 *
 * @req.params.id - MongoId, required.
 *
 * @req.body.file - object.
 * @req.body.file.uri - string, required.
 * @req.body.file.language - enum: ['html', 'css', 'javascript'], required.
 * @req.body.file.content - optional.
 *
 * !!!OR!!!
 *
 * @req.body.files - array of
 *      uri - string, required.
 *      language - enum: ['html', 'css', 'javascript'], required.
 *      content - optional.
 */
routes.post(
  '/:id/create-file',
  authJwt,
  AccessControl.HasAnyRole(['superadmin', 'admin']),
  taskValidation.createFile,
  taskController.createFile,
);

/**
 * Создать задачу.
 *
 * @req.body.title - string, required, min = 3.
 * @req.body.description - string, required, min = 10.
 * @req.body.files - optional, array of objects:
 *      uri - string, required.
 *      language - enum: ['html', 'css', 'javascript'], required.
 *      content - optional.
 */
routes.post(
  '/',
  authJwt,
  AccessControl.HasAnyRole(['superadmin', 'admin']),
  taskValidation.createTask,
  taskController.createTask,
);

/**
 * Получить задачу (могут все авторизованные).
 *
 * @req.params.id - MongoId, required.
 */
routes.get(
  '/:id',
  authJwt,
  taskValidation.getTaskById,
  taskController.getTaskById,
);

/**
 * Получить список задач (могут все авторизованные).
 */
routes.get('/', authJwt, taskController.getTasksList);

/**
 * Обновить Файл в задаче.
 *
 * @req.params.id - MongoId, required.
 *
 * @req.body.file - object.
 * @req.body.file.id - MongoId, required.
 * @req.body.file.uri - string, required.
 * @req.body.file.language - enum: ['html', 'css', 'javascript'], required.
 * @req.body.file.content - optional.
 *
 * !!!OR!!!
 *
 * @req.body.files - array of objects:
 *      id - MongoId, required.
 *      uri - string, required.
 *      language - enum: ['html', 'css', 'javascript'], required.
 *      content - optional.
 */
routes.patch(
  '/:id/update-file',
  authJwt,
  AccessControl.HasAnyRole(['superadmin', 'admin']),
  taskValidation.updateFile,
  taskController.updateFile,
);

/**
 * Обновить задачу. Только title и description.
 *
 * @req.params.id - MongoId, required.
 *
 * @req.body.title - string, required, min = 3.
 * @req.body.description - string, required, min = 10.
 */
routes.patch(
  '/:id',
  authJwt,
  AccessControl.HasAnyRole(['superadmin', 'admin']),
  taskValidation.updateTask,
  taskController.updateTask,
);

/**
 * Удалить Файл из задачи.
 *
 * @req.params.id - MongoId, required.
 *
 * @req.body.file - string, MongoId.
 *
 * !!!OR!!!
 *
 * @req.body.files - array of strings, MongoIds.
 */
routes.delete(
  '/:id/remove-file',
  authJwt,
  AccessControl.HasAnyRole(['superadmin', 'admin']),
  taskValidation.removeFile,
  taskController.removeFile,
);

/**
 * Удалить задачу.
 *
 * @req.params.id - MongoId, required.
 */
routes.delete(
  '/:id',
  authJwt,
  AccessControl.HasAnyRole(['superadmin', 'admin']),
  taskValidation.deleteTask,
  taskController.deleteTask,
);

export default routes;
