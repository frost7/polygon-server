/* eslint-disable no-param-reassign */
import _ from 'lodash';

import Task from './task.model';

export async function createTask(req, res, next) {
  const errorResponse = {
    message: 'Request for file create is invalid.',
    status: 'Failed',
    errors: [],
  };
  let hasAnyError = false;

  let findOneFailed = false;
  const duplicate = await Task.findOne({ title: req.body.title }).catch(() => {
    findOneFailed = true;
  });
  if (findOneFailed) {
    errorResponse.errors.push('Something went wrong with method Task.findOne.');
    res.statusCode = 500;
    return res.json(errorResponse);
  }
  if (duplicate) {
    errorResponse.errors.push('This title already used by another task.');
    res.statusCode = 400;
    return res.json(errorResponse);
  }

  if ('validation' in req.body) {
    if ('testFunction' in req.body.validation) {
      if (typeof req.body.validation.testFunction !== 'string') {
        hasAnyError = true;
        errorResponse.errors.push('Test function must be a string.');
      }
    }
    if ('points' in req.body.validation) {
      let points = parseInt(req.body.validation.points, 10);
      if (points.toString() === 'NaN') {
        hasAnyError = true;
        errorResponse.errors.push('Points must be a valid number.');
      } else {
        req.body.validation.points = points;
      }
    }
    if ('needsJQuery' in req.body.validation) {
      if (typeof req.body.validation.needsJQuery !== 'boolean') {
        hasAnyError = true;
        errorResponse.errors.push('Parameter "needsJQuery" must be a boolean.');
      }
    }
  }

  if ('title' in req.body) {
    if (typeof req.body.title !== 'string') {
      hasAnyError = true;
      errorResponse.errors.push('Title must be a string.');
    } else if (req.body.title.length < 3) {
      hasAnyError = true;
      errorResponse.errors.push('Title is less than 3 symbols.');
    }
  } else {
    hasAnyError = true;
    errorResponse.errors.push('Title required.');
  }

  if ('description' in req.body) {
    if (typeof req.body.description !== 'string') {
      hasAnyError = true;
      errorResponse.errors.push('Description must be a string.');
    } else if (req.body.description.length < 10) {
      hasAnyError = true;
      errorResponse.errors.push('Description is less than 10 symbols.');
    }
  } else {
    hasAnyError = true;
    errorResponse.errors.push('Description required.');
  }

  if ('files' in req.body) {
    req
      .checkBody('files')
      .custom(value => Array.isArray(value))
      .withMessage('Parameter "files" must be an array of objects.');
    req
      .checkBody('files.*.uri', 'File URI must be at least 3 symbols length.')
      .optional()
      .isByteLength({ min: 3 });
    req.checkBody('files.*.uri', 'File URI required.').notEmpty();
    req
      .checkBody(
        'files.*.language',
        'Parameter "language" of file must be equals one of values: "html", "css", "javascript".',
      )
      .optional()
      .isIn(['html', 'css', 'javascript']);
    req
      .checkBody('files.*.language', 'Parameter "language" required.')
      .notEmpty();
  }

  const errors = req.validationErrors();
  if (errors) {
    errors.forEach(item => {
      if (!errorResponse.errors.includes(item.msg))
        errorResponse.errors.push(item.msg);
    });
    res.statusCode = 400;
    return res.json(errorResponse);
  } else if (hasAnyError) {
    res.statusCode = 400;
    return res.json(errorResponse);
  }

  return next();
}

export async function getTaskById(req, res, next) {
  const errorResponse = {
    message: 'Request for get task by Id is invalid.',
    status: 'Failed',
    errors: [],
  };

  const task = await Task.findOne({ _id: req.params.id })
    .populate('user')
    .catch(() => {
      errorResponse.errors.push(`Task id ${req.params.id} is invalid.`);
    });

  if (!task) {
    res.statusCode = 404;
    errorResponse.errors.push(`Task with id ${req.params.id} not found.`);
    return res.json(errorResponse);
  }

  req._task = task;

  return next();
}

export async function createFile(req, res, next) {
  const errorResponse = {
    message: 'Request for file create is invalid.',
    status: 'Failed',
    errors: [],
  };

  const task = await Task.findOne({ _id: req.params.id }).catch(() => {
    errorResponse.errors.push(`Task id ${req.params.id} is invalid.`);
  });

  if (!task) {
    res.statusCode = 404;
    errorResponse.errors.push(`Task with id ${req.params.id} not found.`);
    return res.json(errorResponse);
  }

  req._task = task;

  if (!req.body.file && !req.body.files) {
    errorResponse.errors.push(
      'Request must has one of parameters: "file", or "files".',
    );
    res.statusCode = 400;
    return res.json(errorResponse);
  }

  if (req.body.file) {
    req
      .checkBody('file', 'Parameter "file" must be an object.')
      .custom(value => _.isPlainObject(value));
    req
      .checkBody('file.uri', 'File URI must be at least 3 symbols length.')
      .optional()
      .isByteLength({ min: 3 });
    req.checkBody('file.uri', 'File URI required.').notEmpty();
    req
      .checkBody(
        'file.language',
        'Parameter "language" of file must be equals one of values: "html", "css", "javascript".',
      )
      .optional()
      .isIn(['html', 'css', 'javascript']);
    req.checkBody('file.language', 'Parameter "language" required.').notEmpty();
  } else if (req.body.files) {
    req
      .checkBody('files', 'Parameter "files" must be an array of objects.')
      .custom(value => _.isArray(value) && value.length > 0);
    req
      .checkBody('files.*.uri', 'File URI must be at least 3 symbols length.')
      .optional()
      .isByteLength({ min: 3 });
    req.checkBody('files.*.uri', 'File URI required.').notEmpty();
    req
      .checkBody(
        'files.*.language',
        'Parameter "language" of file must be equals one of values: "html", "css", "javascript".',
      )
      .optional()
      .isIn(['html', 'css', 'javascript']);
    req
      .checkBody('files.*.language', 'Parameter "language" required.')
      .notEmpty();
  }

  const errors = req.validationErrors();
  if (errors) {
    errors.forEach(item => {
      if (!errorResponse.errors.includes(item.msg))
        errorResponse.errors.push(item.msg);
    });
    res.statusCode = 400;
    return res.json(errorResponse);
  }

  return next();
}

export async function updateFile(req, res, next) {
  const errorResponse = {
    message: 'Request for file update is invalid.',
    status: 'Failed',
    errors: [],
  };

  const task = await Task.findOne({ _id: req.params.id }).catch(() => {
    errorResponse.errors.push(`Task id ${req.params.id} is invalid.`);
  });

  if (!task) {
    res.statusCode = 404;
    errorResponse.errors.push(`Task with id ${req.params.id} not found.`);
    return res.json(errorResponse);
  }

  req._task = task;

  if (!req.body.file && !req.body.files) {
    errorResponse.errors.push(
      'Request must has one of parameters: "file", or "files".',
    );
    res.statusCode = 400;
    return res.json(errorResponse);
  }

  if (req.body.file) {
    req
      .checkBody('file', 'Parameter "file" must be an object.')
      .custom(value => _.isPlainObject(value));
    req
      .checkBody('file.id', 'File Id missed or invalid.')
      .notEmpty()
      .isMongoId();
    req
      .checkBody('file.uri', 'File URI must be at least 3 symbols length.')
      .optional()
      .isByteLength({ min: 3 });
    req
      .checkBody(
        'file.language',
        'Parameter "language" of file must be equals one of values: "html", "css", "javascript".',
      )
      .optional()
      .isIn(['html', 'css', 'javascript']);
  } else if (req.body.files) {
    req
      .checkBody('files', 'Parameter "files" must be an array of objects.')
      .custom(value => _.isArray(value) && value.length > 0);
    req
      .checkBody('files.*.id', 'File Id missed or invalid.')
      .notEmpty()
      .isMongoId();
    req
      .checkBody('files.*.uri', 'File URI must be at least 3 symbols length.')
      .optional()
      .isByteLength({ min: 3 });
    req
      .checkBody(
        'files.*.language',
        'Parameter "language" of file must be equals one of values: "html", "css", "javascript".',
      )
      .optional()
      .isIn(['html', 'css', 'javascript']);
  }

  const errors = req.validationErrors();
  if (errors) {
    errors.forEach(item => {
      if (!errorResponse.errors.includes(item.msg))
        errorResponse.errors.push(item.msg);
    });
    res.statusCode = 400;
    return res.json(errorResponse);
  }

  return next();
}

export async function updateTask(req, res, next) {
  const errorResponse = {
    message: 'Request for task update is invalid.',
    status: 'Failed',
    errors: [],
  };
  let hasAnyError = false;

  const task = await Task.findOne({ _id: req.params.id }).catch(() => {
    errorResponse.errors.push(`Task id ${req.params.id} is invalid.`);
  });

  if (!task) {
    res.statusCode = 404;
    errorResponse.errors.push(`Task with id ${req.params.id} not found.`);
    return res.json(errorResponse);
  }

  req._task = task;

  if ('validation' in req.body) {
    if ('testFunction' in req.body.validation) {
      if (typeof req.body.validation.testFunction !== 'string') {
        hasAnyError = true;
        errorResponse.errors.push('Test function must be a string.');
      }
    }
    if ('points' in req.body.validation) {
      let points = parseInt(req.body.validation.points, 10);
      if (points.toString() === 'NaN') {
        hasAnyError = true;
        errorResponse.errors.push('Points must be a valid number.');
      } else {
        req.body.validation.points = points;
      }
    }
    if ('needsJQuery' in req.body.validation) {
      if (typeof req.body.validation.needsJQuery !== 'boolean') {
        hasAnyError = true;
        errorResponse.errors.push('Parameter "needsJQuery" must be a boolean.');
      }
    }
  }

  if ('title' in req.body) {
    if (typeof req.body.title !== 'string') {
      hasAnyError = true;
      errorResponse.errors.push('Title must be a string.');
    } else if (req.body.title.length < 3) {
      hasAnyError = true;
      errorResponse.errors.push('Title is less than 3 symbols.');
    }
  }

  if ('description' in req.body) {
    if (typeof req.body.description !== 'string') {
      hasAnyError = true;
      errorResponse.errors.push('Description must be a string.');
    } else if (req.body.description.length < 10) {
      hasAnyError = true;
      errorResponse.errors.push('Description is less than 10 symbols.');
    }
  }

  const errors = req.validationErrors();
  if (errors) {
    errors.forEach(item => {
      if (!errorResponse.errors.includes(item.msg))
        errorResponse.errors.push(item.msg);
    });
    res.statusCode = 400;
    return res.json(errorResponse);
  } else if (hasAnyError) {
    res.statusCode = 400;
    return res.json(errorResponse);
  }

  return next();
}

export async function removeFile(req, res, next) {
  const errorResponse = {
    message: 'Request for file delete is invalid.',
    status: 'Failed',
    errors: [],
  };

  const task = await Task.findOne({ _id: req.params.id }).catch(() => {
    errorResponse.errors.push(`Task id ${req.params.id} is invalid.`);
  });

  if (!task) {
    res.statusCode = 404;
    errorResponse.errors.push(`Task with id ${req.params.id} not found.`);
    return res.json(errorResponse);
  }

  req._task = task;

  if (!req.body.file && !req.body.files) {
    errorResponse.errors.push(
      'Request must has one of parameters: "file", or "files".',
    );
    res.statusCode = 400;
    return res.json(errorResponse);
  }

  if (req.body.file) {
    if (!task.files.includes(req.body.file)) {
      errorResponse.errors.push(
        `No such file (id === ${req.body.file}) in files array of Task "${
          task.title
        }".`,
      );
      res.statusCode = 404;
      return res.json(errorResponse);
    }
  } else if (req.body.files) {
    const len = req.body.files.length;
    for (let i = 0; i < len; i++) {
      const pos = task.files.findIndex(
        item => item._id.toJSON() === req.body.files[i],
      );
      if (pos === -1) {
        errorResponse.errors.push(
          `One or more of requested for deleting files not found in Task "${
            task.title
          }".`,
        );
        res.statusCode = 404;
        return res.json(errorResponse);
      }
    }
  }

  return next();
}

export async function deleteTask(req, res, next) {
  const errorResponse = {
    message: 'Request for task delete is invalid.',
    status: 'Failed',
    errors: [],
  };

  const task = await Task.findOne({ _id: req.params.id }).catch(() => {
    errorResponse.errors.push(`Task id ${req.params.id} is invalid.`);
  });

  if (!task) {
    res.statusCode = 404;
    errorResponse.errors.push(`Task with id ${req.params.id} not found.`);
    return res.json(errorResponse);
  }

  req._task = task;

  return next();
}
