import mongoose, { Schema } from 'mongoose';
import slug from 'slug';
import uniqueValidator from 'mongoose-unique-validator';

const MockFileSchema = new Schema({
  uri: {
    type: String,
    required: [true, 'File address is required.'],
    trim: true,
    minLength: [3, 'File address must be longer.'],
  },
  language: {
    type: String,
    trim: true,
    required: [true, 'Language is required.'],
    enum: ['html', 'css', 'javascript'],
  },
  content: {
    type: String,
  },
});

const TaskSchema = new Schema(
  {
    title: {
      type: String,
      trim: true,
      required: [true, 'Title is required.'],
      minLength: [3, 'Title needs to be longer.'],
      unique: true,
    },
    slug: {
      type: String,
      trim: true,
      lowercase: true,
    },
    description: {
      type: String,
      trim: true,
      required: [true, 'Description is required.'],
      minLength: [10, 'Description must be longer.'],
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    files: [MockFileSchema],
    testFunction: {
      // Validation.
      type: String,
      default: '',
    },
    points: {
      // Validation.
      type: Number,
      default: 5,
    },
    needsJQuery: {
      // Validation.
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true },
);

TaskSchema.plugin(uniqueValidator, {
  message: '{VALUE} is already taken.',
});

TaskSchema.pre('validate', function(next) {
  this._slugify();
  return next();
});

TaskSchema.methods = {
  _slugify() {
    this.slug = slug(this.title);
  },
  toJSON() {
    return {
      id: this._id,
      title: this.title,
      description: this.description,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      slug: this.slug,
      validation: {
        testFunction: this.testFunction ? this.testFunction : '',
        points: this.points || this.points === 0 ? this.points : 5,
        needsJQuery: !!this.needsJQuery,
      },
      user: this.user
        ? {
            id: this.user._id,
            userName: this.user.userName,
            lastName: this.user.lastName,
            firstName: this.user.firstName,
            email: this.user.email,
            roles: this.user.roles,
          }
        : null,
      files: this.files
        ? this.files.map(item => {
            return {
              uri: item.uri,
              language: item.language,
              content: item.content,
              id: item._id,
              task: item.task,
            };
          })
        : [],
    };
  },
};

TaskSchema.statics = {
  createTask(args, user) {
    return this.create({
      ...args,
      user,
    });
  },
  list({ limit = 5, skip = 0 } = {}) {
    return this.find()
      .sort({ createdAt: -1 })
      .skip(skip)
      .limit(limit)
      .populate('user');
  },
  async getTasksByFile(fileId) {
    return await this.find({ files: fileId }).populate('user');
  },
};

export default mongoose.model('Task', TaskSchema);
