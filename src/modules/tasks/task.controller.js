/* eslint-disable no-console */
import HTTPStatus from 'http-status';

import Task from './task.model';

export async function createTask(req, res) {
  try {
    const newTaskData = {
      title: req.body.title,
      description: req.body.description,
      files: req.body.files ? req.body.files : [],
    };
    if ('validation' in req.body) {
      if ('testFunction' in req.body.validation) {
        newTaskData.testFunction = req.body.validation.testFunction;
      }
      if ('points' in req.body.validation) {
        newTaskData.points = req.body.validation.points;
      }
      if ('needsJQuery' in req.body.validation) {
        newTaskData.needsJQuery = req.body.validation.needsJQuery;
      }
    }
    const task = new Task({ ...newTaskData, user: req.user._id });
    const saved = await task.save();
    await Task.populate(saved, { path: 'user' });
    return res.status(HTTPStatus.CREATED).json(saved);
  } catch (err) {
    return res.status(HTTPStatus.BAD_REQUEST).json(err);
  }
}

/**
 * Принимает объект req.body.file, если надо добавить один файл или
 * массив req.body.files, если надо добавить несколько файлов.
 * @returns {Promise<*>}
 */
export async function createFile(req, res) {
  try {
    const task = req._task ? req._task : await Task.findById(req.params.id);
    if (req.body.file) {
      const newFile = {
        uri: req.body.file.uri,
        language: req.body.file.language,
        content: req.body.file.content,
      };
      task.files.push(newFile);
    } else if (req.body.files && req.body.files.length > 0) {
      const len = req.body.files.length;
      for (let i = 0; i < len; i++) {
        task.files.push({ ...req.body.files[i] });
      }
    }
    const saved = await task.save();
    await Task.populate(saved, { path: 'user' });
    return res.status(HTTPStatus.CREATED).json(saved);
  } catch (err) {
    return res.status(HTTPStatus.BAD_REQUEST).json(err);
  }
}

export async function getTaskById(req, res) {
  try {
    const task = req._task
      ? req._task
      : await Task.findById(req.params.id)
          .populate('user')
          .catch(
            () => new Error(`Can not find task with id === ${req.params.id}.`),
          );
    return res.status(HTTPStatus.OK).json(task.toJSON());
  } catch (err) {
    return res.status(HTTPStatus.BAD_REQUEST).json(err);
  }
}

export async function getTasksList(req, res) {
  const limit = parseInt(req.query.limit, 10);
  const skip = parseInt(req.query.skip, 10);
  try {
    const tasks = await Task.list({ limit, skip });
    return res.status(HTTPStatus.OK).json(tasks);
  } catch (err) {
    return res.status(HTTPStatus.BAD_REQUEST).json(err);
  }
}

/**
 * Принимает объект req.body.file, если надо обновить один файл или
 * массив req.body.files, если надо обновить несколько файлов.
 * @returns {Promise<*>}
 */
export async function updateFile(req, res) {
  try {
    const task = req._task
      ? req._task
      : await Task.findById(req.params.id).catch(
          () => new Error(`Can not find task with id === ${req.params.id}.`),
        );
    if (req.body.file) {
      const file = task.files.id(req.body.file.id);
      if (file) {
        if (req.body.file.uri) file.uri = req.body.file.uri;
        if (req.body.file.language) file.language = req.body.file.language;
        if (req.body.file.content) file.content = req.body.file.content;
        file.save({ suppressWarning: true });
      } else {
        return res.status(HTTPStatus.BAD_REQUEST).json({
          message: `File with id === "${
            req.body.file.id
          }" does not exist in task "${task.title}".`,
          status: 'Failed',
        });
      }
    } else if (req.body.files && req.body.files.length > 0) {
      const len = req.body.files.length;
      for (let i = 0; i < len; i++) {
        const file = task.files.id(req.body.files[i].id);
        if (file) {
          if (req.body.files[i].uri) file.uri = req.body.files[i].uri;
          if (req.body.files[i].language)
            file.language = req.body.files[i].language;
          if (req.body.files[i].content)
            file.content = req.body.files[i].content;
          file.save({ suppressWarning: true });
        } else {
          return res.status(HTTPStatus.BAD_REQUEST).json({
            message: `File with id === "${
              req.body.files[i].id
            }" does not exist in task "${task.title}".`,
            status: 'Failed',
          });
        }
      }
    }
    const saved = await task.save();
    await Task.populate(saved, { path: 'user' });
    return res.status(HTTPStatus.OK).json(saved);
  } catch (err) {
    return res.status(HTTPStatus.BAD_REQUEST).json(err);
  }
}

/**
 * Обновляет только title и description.
 * @returns {Promise<*>}
 */
export async function updateTask(req, res) {
  try {
    const task = req._task
      ? req._task
      : await Task.findById(req.params.id).catch(
          () => new Error(`Can not find task with id === ${req.params.id}.`),
        );
    // if (!task.user.equals(req.user._id)) { // Не даёт обновить юзерам, не являющимися авторами документа.
    //   return res.sendStatus(HTTPStatus.UNAUTHORIZED);
    // }
    if ('title' in req.body) task.title = req.body.title;
    if ('description' in req.body) task.description = req.body.description;
    if ('validation' in req.body) {
      if ('testFunction' in req.body.validation) {
        task.testFunction = req.body.validation.testFunction;
      }
      if ('points' in req.body.validation) {
        task.points = req.body.validation.points;
      }
      if ('needsJQuery' in req.body.validation) {
        task.needsJQuery = req.body.validation.needsJQuery;
      }
    }
    await task.save();
    await Task.populate(task, { path: 'user' });
    return res.status(HTTPStatus.OK).json(task);
  } catch (err) {
    return res.status(HTTPStatus.BAD_REQUEST).json(err);
  }
}

/**
 * Принимает объект req.body.file, усли надо удалить один файл или
 * массив req.body.files, если надо удалить несколько файлов.
 * @returns {Promise<*>}
 */
export async function removeFile(req, res) {
  try {
    const task = req._task
      ? req._task
      : await Task.findById(req.params.id).catch(
          () => new Error(`Can not find task with id === ${req.params.id}.`),
        );
    if (req.body.file && typeof req.body.file === 'string') {
      task.files.id(req.body.file).remove();
    } else if (req.body.files && req.body.files.length > 0) {
      const len = req.body.files.length;
      for (let i = 0; i < len; i++) {
        task.files.id(req.body.files[i]).remove();
      }
    }
    const saved = await task.save();
    await Task.populate(saved, { path: 'user' });
    return res.status(HTTPStatus.CREATED).json(saved);
  } catch (err) {
    return res.status(HTTPStatus.BAD_REQUEST).json(err);
  }
}

export async function deleteTask(req, res) {
  try {
    const task = req._task
      ? req._task
      : await Task.findById(req.params.id).catch(
          () => new Error(`Can not find task with id === ${req.params.id}.`),
        );
    const title = task.title;
    await task.remove();
    return res.status(HTTPStatus.OK).json({
      message: `Task '${title}' successfully deleted.`,
      status: 'OK',
    });
  } catch (err) {
    return res.status(HTTPStatus.BAD_REQUEST).json(err);
  }
}
