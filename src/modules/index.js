import userRoutes from './users/user.routes';
import taskRoutes from './tasks/task.routes';
import permissionRoutes from './permissions/permission.routes';
import roleRoutes from './roles/role.routes';

export default app => {
  app.use('/api/v1/users', userRoutes);
  app.use('/api/v1/tasks', taskRoutes);
  app.use('/api/v1/permissions', permissionRoutes);
  app.use('/api/v1/roles', roleRoutes);
};
