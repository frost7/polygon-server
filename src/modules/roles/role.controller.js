/* eslint-disable no-param-reassign */
import HTTPStatus from 'http-status';

import Role from './role.model';
import Permission from '../permissions/permission.model';

export async function createRole(req, res) {
  try {
    const role = await Role.createRole(req.body);
    return res.status(HTTPStatus.CREATED).json(role);
  } catch (err) {
    return res.status(HTTPStatus.BAD_REQUEST).json(err);
  }
}

export async function getRoleById(req, res) {
  try {
    const role = req._role
      ? req._role
      : await Role.findById(req.params.id)
          .populate('permissions')
          .catch(
            () => new Error(`Can not find role with id === ${req.params.id}.`),
          );
    return res.status(HTTPStatus.OK).json(role.toJSON());
  } catch (err) {
    return res.status(HTTPStatus.BAD_REQUEST).json(err);
  }
}

export async function getRolesList(req, res) {
  try {
    const limit = parseInt(req.query.limit, 10);
    const skip = parseInt(req.query.skip, 10);
    const roles = await Role.list({ limit, skip });
    return res.status(HTTPStatus.OK).json(roles);
  } catch (err) {
    return res.status(HTTPStatus.BAD_REQUEST).json(err);
  }
}

export async function renameRole(req, res) {
  try {
    const role = req._role
      ? req._role
      : await Role.findById(req.params.id)
          .populate('permissions')
          .catch(
            () => new Error(`Can not find role with id === ${req.params.id}.`),
          );
    role.name = req.body.name;
    return res.status(HTTPStatus.OK).json(await role.save());
  } catch (err) {
    return res.status(HTTPStatus.BAD_REQUEST).json(err);
  }
}

export async function deleteRole(req, res) {
  try {
    const role = req._role
      ? req._role
      : await Role.findById(req.params.id)
          .populate('permissions')
          .catch(
            () => new Error(`Can not find role with id === ${req.params.id}.`),
          );
    const name = role.name;
    await role.remove();
    return res.status(HTTPStatus.OK).json({
      message: `Role ${name} successfully deleted.`,
      status: 'OK',
    });
  } catch (err) {
    return res.status(HTTPStatus.BAD_REQUEST).json(err);
  }
}

/**
 * Превращает массив разрешений в массив пар типа
 *
 * [
 *   [ "action1", "subject1" ],
 *   [ "action2", "subject2" ],
 * ]
 *
 * Ненайденные разрешения создаёт.
 *
 * @returns {Promise<*>}
 */
export async function transformPermissions(req, res, next) {
  try {
    const permissions = [...req.body.permissions];
    if (permissions.length === 0) {
      return next();
    }
    req.body.permissions = await Permission.processArrayOfPermissions(
      permissions,
      'createNew',
    );
    return next();
  } catch (err) {
    return res.status(HTTPStatus.BAD_REQUEST).json(err);
  }
}

/**
 * Превращает массив разрешений req.body.permissions в массив пар типа
 *
 * [
 *   [ "action1", "subject1" ],
 *   [ "action2", "subject2" ],
 * ]
 *
 * Ненайденные разрешения пропускает.
 *
 * @returns {Promise<*>}
 */
export async function transformPermissionsSkipInvalid(req, res, next) {
  try {
    const permissions = [...req.body.permissions];
    if (permissions.length === 0) {
      return next();
    }
    req.body.permissions = await Permission.processArrayOfPermissions(
      permissions,
      'skip',
    );
    return next();
  } catch (err) {
    return res.status(HTTPStatus.BAD_REQUEST).json(err);
  }
}

/**
 * Добавляет разрешения в Роль.
 */
export async function addPermissions(req, res) {
  try {
    const role = await Role.updatePermissions(
      req.params.id,
      req.body.permissions,
    ).catch(() => new Error(`Can not update role with id "${req.params.id}".`));
    if (!role) {
      return res.status(HTTPStatus.NOT_FOUND).json({
        message: `Role with ID '${req.params.id}' not found.`,
        status: 'Failed',
      });
    }
    return res.status(HTTPStatus.CREATED).json(role);
  } catch (err) {
    return res.status(HTTPStatus.BAD_REQUEST).json(err);
  }
}

/**
 * Удаляет разрешения из Роли.
 */
export async function removePermissions(req, res) {
  try {
    const role = await Role.deletePermissions(
      req.params.id,
      req.body.permissions,
    ).catch(() => new Error(`Can not update role with id "${req.params.id}".`));
    if (!role) {
      return res.status(HTTPStatus.NOT_FOUND).json({
        message: `Role with ID '${req.params.id}' not found.`,
        status: 'Failed',
      });
    }
    return res.status(HTTPStatus.CREATED).json(role);
  } catch (err) {
    return res.status(HTTPStatus.BAD_REQUEST).json(err);
  }
}
