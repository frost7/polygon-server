/**
 * Принимает массив строк. Возвращает новый массив, где каждый элемент
 * из старого встречается лишь 1 раз.
 * @param array
 * @returns {Array}
 */
export function deleteDoubles(array) {
  const unique = [];
  const len = array.length;
  for (let i = 0; i < len; i++) {
    const current = array[i];
    if (typeof current !== 'string') continue;
    if (!unique.includes(current)) {
      unique.push(current);
    }
  }
  return unique;
}

export function deleteDoublesOfObjectsByField(array, field = 'id') {
  const unique = [];
  const uniqueIDs = [];
  const len = array.length;
  for (let i = 0; i < len; i++) {
    const current = array[i];
    if (typeof current !== 'object') continue;
    if (!current[field]) continue;
    if (!uniqueIDs.includes(current[field])) {
      unique.push(current);
      uniqueIDs.push(current[field]);
    }
  }
  return unique;
}
