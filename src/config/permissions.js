export const ROLES = {
  SUPERADMIN: 'superadmin',
  ADMIN: 'admin',
  STUDENT: 'student',
  READONLY: 'readonly',
};
export const defaultRole = ROLES.STUDENT;
export const permissions = {};
permissions[ROLES.SUPERADMIN] = [
  ['create', 'task'],
  ['read', 'task'],
  ['update', 'task'],
  ['delete', 'task'],
  ['list', 'task'],
  ['create', 'user'],
  ['read', 'user'],
  ['update', 'user'],
  ['delete', 'user'],
  ['list', 'user'],
  ['create', 'permission'],
  ['read', 'permission'],
  ['update', 'permission'],
  ['delete', 'permission'],
  ['list', 'permission'],
  ['create', 'role'],
  ['read', 'role'],
  ['update', 'role'],
  ['delete', 'role'],
  ['list', 'role'],
];
permissions[ROLES.ADMIN] = [
  ['create', 'task'],
  ['read', 'task'],
  ['update', 'task'],
  ['delete', 'task'],
  ['list', 'task'],
  ['create', 'user'],
  ['read', 'user'],
  ['update', 'user'],
  ['delete', 'user'],
  ['list', 'user'],
  ['read', 'role'],
  ['list', 'role'],
];
permissions[ROLES.STUDENT] = [['read', 'task'], ['list', 'task']];
permissions[ROLES.READONLY] = [
  ['read', 'user'],
  ['list', 'user'],
  ['read', 'task'],
  ['list', 'task'],
  ['read', 'permission'],
  ['list', 'permission'],
  ['read', 'role'],
  ['list', 'role'],
];
