const devConfig = {
  MONGO_URL: 'mongodb://localhost/polygon-dev',
  JWT_SECRET: 'kw2o0893tjhPOIHJO8yfgow34230efjh',
};

const testConfig = {
  MONGO_URL: 'mongodb://localhost/polygon-test',
};

const prodConfig = {
  MONGO_URL: 'mongodb://localhost/polygon-prod',
};

const defaultConfig = {
  PORT: process.env.PORT || 3000,
};

function envConfig(env) {
  switch (env) {
    case 'development':
      return devConfig;
    case 'test':
      return testConfig;
    default:
      return prodConfig;
  }
}

export default {
  ...defaultConfig,
  ...envConfig(process.env.NODE_ENV),
};
