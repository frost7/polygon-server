/* eslint-disable no-console */
import express from 'express';
import './database';
import middlewareConfig from '../config/middleware';

import Role from '../modules/roles/role.model';
import User from '../modules/users/user.model';
import Permission from '../modules/permissions/permission.model';
import { ROLES, permissions } from './permissions';
import { asyncForEach } from '../helpers/async-foreach';

const app = express();
middlewareConfig(app);

console.log('\x1b[36m%s\x1b[0m', 'RBAC Bootstrap activated.');

const roleNames = [];

for (const key in ROLES) {
  roleNames.push(ROLES[key]);
}

checkRolesExisting(roleNames)
  .then(roles => {
    console.log('\x1b[35m%s\x1b[0m', 'Roles exist:');
    roles.forEach(item => console.log('\x1b[32m\x1b[0m', item));
    console.log(' ');
    return roles;
  })
  .then(async roles => {
    let permissionsDocs = [];
    await asyncForEach(roles, async item => {
      permissionsDocs = await transformPermissions(permissions[item[0]]);
      const permissionIDs = permissionsDocs.map(perm => perm._id);
      let role;
      if (!item[1]) {
        await Role.create({
          name: item[0],
          permissions: permissionIDs,
        });
        role = await Role.findOne({ name: item[0] }).populate('permissions');
        console.log('\x1b[35m%s\x1b[0m', 'Role created: ');
      } else {
        role = await Role.findOneAndUpdate(
          {
            name: item[0],
          },
          { permissions: permissionIDs },
          { new: true },
        ).populate('permissions');
        console.log('\x1b[35m%s\x1b[0m', 'Role updated: ');
      }
      console.log('\x1b[36m%s\x1b[0m', `Role: ${role.name}`);
      console.log(
        '\x1b[32m',
        role.permissions.map(perm => [perm.action, perm.subject]),
      );
      console.log('\x1b[37m', ' ');
    });
    return await Role.find().populate('permissions');
  })
  .then(roles => {
    createAdmins(roles);
  });

export async function getRoleByName(roleName) {
  return await Role.findOne({ name: roleName }).populate('permissions');
}

export async function checkRolesExisting(roleList) {
  const roles = [];
  for (const item of roleList) {
    const role = await getRoleByName(item);
    roles.push([item, !!role]);
  }
  return roles;
}

export async function transformPermissions(perms) {
  return await Permission.processArrayOfPermissions(perms, 'createNew');
}

export async function createAdmins(roles) {
  const superadmin = await User.findOne({ userName: 'AlphaBlack' });
  const superadminRole = roles.find(item => item.name === ROLES.SUPERADMIN);
  const adminRole = roles.find(item => item.name === ROLES.ADMIN);
  if (!superadmin) {
    console.log(
      '\x1b[31m%s\x1b[0m',
      'Default SuperAdmin AlphaBlack not found.',
    );
    const newSuperAdmin = await User.create({
      userName: 'AlphaBlack',
      firstName: 'Mikhail',
      lastName: 'Filchushkin',
      roles: [superadminRole._id],
      password: 'HelloWorld1',
      email: 'webestet@gmail.com',
    });
    console.log(
      '\x1b[35m%s\x1b[0m',
      `Default SuperAdmin ${newSuperAdmin.firstName} ${
        newSuperAdmin.lastName
      } is created.`,
    );
  } else {
    console.log('\x1b[32m%s\x1b[0m', 'Default SuperAdmin AlphaBlack found.');
    const updatedSuperAdmin = await User.findOneAndUpdate(
      { userName: 'AlphaBlack' },
      {
        roles: [superadminRole._id],
      },
    ).populate({ path: 'roles', populate: { path: 'permissions' } });
    console.log(
      '\x1b[32m%s\x1b[0m',
      `Default SuperAdmin ${updatedSuperAdmin.firstName} ${
        updatedSuperAdmin.lastName
      } roles updated. Roles: '${superadminRole.name}'.`,
    );
  }
  const admin = await User.findOne({ userName: 'BravoRed' });
  if (!admin) {
    console.log('\x1b[31m%s\x1b[0m', 'Default Admin BravoRed not found.');
    const newAdmin = await User.create({
      userName: 'BravoRed',
      firstName: 'Mikhail',
      lastName: 'Filchushkin',
      roles: [adminRole._id],
      password: 'HelloWorld1',
      email: 'info@webestet.ru',
    });
    console.log(
      '\x1b[35m%s\x1b[0m',
      `Default Admin ${newAdmin.firstName} ${newAdmin.lastName} is created.`,
    );
  } else {
    console.log('\x1b[32m%s\x1b[0m', 'Default Admin BravoRed found.');
    const updatedAdmin = await User.findOneAndUpdate(
      { userName: 'BravoRed' },
      {
        roles: [adminRole._id],
      },
    ).populate({ path: 'roles', populate: { path: 'permissions' } });
    console.log(
      '\x1b[32m%s\x1b[0m',
      `Default Admin ${updatedAdmin.firstName} ${
        updatedAdmin.lastName
      } roles updated. Roles: '${adminRole.name}'.`,
    );
  }
}
