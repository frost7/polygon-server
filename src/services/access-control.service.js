import HTTPStatus from 'http-status';

/**
 * Принимает строку, ищет вхождение роли в юзере.
 * @param role - string
 * @returns {*|Function}
 * @constructor
 */
export function HasRole(role) {
  return (
    HasRole[role] ||
    (HasRole[role] = function(req, res, next) {
      try {
        const hasRole = req.user.roles.find(item => item.name === role);
        if (hasRole) {
          return next();
        } else {
          return res.status(HTTPStatus.FORBIDDEN).json({
            message: `You have no role '${role}' for this action.`,
            status: 'Failed',
          });
        }
      } catch (err) {
        return res.status(HTTPStatus.FORBIDDEN).json(err);
      }
    })
  );
}

/**
 * Принимает массив строк. Ищет совпадение с одной из ролей.
 * @param roles - Array<String>
 * @returns {*|Function}
 * @constructor
 */

export function HasAnyRole(roles) {
  return (
    HasAnyRole[roles] ||
    (HasAnyRole[roles] = function(req, res, next) {
      try {
        const len = roles.length;
        let hasTargetRole = false;
        if (len === 0) {
          return next();
        }
        let roleNames = `'${roles[0]}'`;
        for (let i = 0; i < len; i++) {
          if (i !== 0) {
            roleNames += `, '${roles[i]}'`;
          }
          const hasRole = req.user.roles.find(item => item.name === roles[i]);
          if (hasRole) {
            hasTargetRole = true;
            break;
          }
        }
        if (hasTargetRole) {
          return next();
        } else {
          return res.status(HTTPStatus.FORBIDDEN).json({
            message: `You have no one of roles ${roleNames} for this action.`,
            status: 'Failed',
          });
        }
      } catch (err) {
        return res.status(HTTPStatus.FORBIDDEN).json(err);
      }
    })
  );
}
